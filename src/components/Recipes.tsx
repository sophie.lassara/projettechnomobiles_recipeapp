import React from "react";
import { Recette } from "../model/Recette";
import { IonList, IonItem, IonLabel } from "@ionic/react";

interface Props {
  recettes: Recette[];
  filtre: string;
  filtreCategory: string;
}

const Recettes = ({ recettes, filtre, filtreCategory }: Props) => (
  <IonList>
    {recettes.filter((recette) => recette.id.includes(filtre) && recette.category.includes(filtreCategory)).map((recette) => (
      <IonItem>
        <IonLabel>
          <h2>{recette.name}</h2>
          <h3>{recette.category}</h3>
        </IonLabel>
      </IonItem>
    ))}
  </IonList>
);

export default Recettes;