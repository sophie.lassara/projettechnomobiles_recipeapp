import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonNote,
} from '@ionic/react';

import React from 'react';
import { useLocation } from 'react-router-dom';
import { restaurantOutline, restaurantSharp, nutritionOutline, nutritionSharp, pizzaOutline, pizzaSharp, iceCreamOutline, iceCreamSharp } from 'ionicons/icons';
import './Menu.css';

interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}

const appPages: AppPage[] = [
  {
    title: 'Accueil',
    url: '/page/Accueil',
    iosIcon: restaurantOutline,
    mdIcon: restaurantSharp
  },
  {
    title: 'Entrées',
    url: '/page/Entree',
    iosIcon: nutritionOutline,
    mdIcon: nutritionSharp
  },
  {
    title: 'Plats',
    url: '/page/Plat',
    iosIcon: pizzaOutline,
    mdIcon: pizzaSharp
  },
  {
    title: 'Desserts',
    url: '/page/Dessert',
    iosIcon: iceCreamOutline,
    mdIcon: iceCreamSharp
  }
];


const Menu: React.FC = () => {
  const location = useLocation();

  return (
    <IonMenu contentId="main" type="overlay">
      <IonContent>
        <IonList id="inbox-list">
          <IonListHeader>Les Recettes de Sophie</IonListHeader>
          <IonNote>Projet Technologies Mobiles</IonNote>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                  <IonIcon slot="start" icon={appPage.iosIcon} />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
