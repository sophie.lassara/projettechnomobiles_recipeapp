import React, { useState } from "react";
import { IonSearchbar } from "@ionic/react";
import "./ExploreContainer.css";
import Recettes from "./Recipes";
import recipe from "../model/data/recettes.json";

interface ContainerProps {
  name: string;
  filtreCategory : string;
}

const ExploreContainer: React.FC<ContainerProps> = ({ name , filtreCategory}) => {
  const [searchText, setSearchText] = useState('');
  return (
    <>
      <IonSearchbar value={searchText} onIonChange={e => setSearchText(e.detail.value!)} placeholder="filtre recette">
      </IonSearchbar>
      <Recettes recettes={recipe} filtre={searchText} filtreCategory={filtreCategory}></Recettes>
    </>
  );
};

export default ExploreContainer;
